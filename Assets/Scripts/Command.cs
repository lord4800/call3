[System.Serializable]
public class Command
{
    public CommandType CommandType;
    public float Arg;

    public Command(CommandType commandType, float arg)
    {
        this.CommandType = commandType;
        this.Arg = arg;
    }
}

