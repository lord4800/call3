using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellWidget : MonoBehaviour
{
    [SerializeField] private float maxCellPower;

    [SerializeField] private Gradient gradient;
    [SerializeField] private Renderer renderer;

    private Cell cell;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        cell = new Cell(maxCellPower);
        cell.UpdatePower += OnUpdatePower;
        renderer.material.color = gradient.Evaluate(cell.CurrentChargePercent);
    }

    private void OnDisable()
    {
        if (cell != null) cell.UpdatePower -= OnUpdatePower;
    }

    private void OnUpdatePower()
    {
        renderer.material.color = gradient.Evaluate(cell.CurrentChargePercent);
    }

    public void Charge(float power)
    {
        cell.Charge(power);
    }

    public void Discharge(float power)
    {
        cell.Discharge(power);
    }

    [Sirenix.OdinInspector.Button("Test minus 10")]
    private void TestMinus10()
    {
        Discharge(10f);
    }

    [Sirenix.OdinInspector.Button("Test plus 5")]
    private void TestCharge5()
    {
        Charge(5f);
    }
}
