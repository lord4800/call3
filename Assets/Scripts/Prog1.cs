using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prog1 : MonoBehaviour
{
    [SerializeField] private RobotBehaviour robot;
    [SerializeField] private List<Command> commands;

    [Sirenix.OdinInspector.Button("Start Execute")]
    public void StartExecute()
    {
        robot.SaveCommands(commands);
    }
}
