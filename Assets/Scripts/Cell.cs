using System;

public class Cell
{
    public Action UpdatePower;

    private float maxPower;
    private float currentPower;

    public float CurrentChargePercent
    {
        get { return currentPower / maxPower; }
    }

    public Cell(float maxPower)
    {
        this.maxPower = maxPower;
        currentPower = maxPower;
    }

    public Cell(float maxPower, float currentPower)
    {
        this.maxPower = maxPower;
        this.currentPower = currentPower;
    }

    public void Charge(float power)
    {
        currentPower += power;
        if (currentPower > maxPower)
            currentPower = maxPower;
        if (UpdatePower != null) UpdatePower();
    }

    //for succsess if we have enoght power
    public bool Discharge(float power)
    {
        currentPower -= power;
        if (currentPower < 0f)
        {
            currentPower = 0f;
            if (UpdatePower != null) UpdatePower();
            return false;
        }
        if (UpdatePower != null) UpdatePower();
        return true;
    }
}
