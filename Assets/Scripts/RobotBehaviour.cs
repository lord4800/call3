using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

public class RobotBehaviour : MonoBehaviour
{
    private Action EndStep;

    [SerializeField] private float gridSize;
    [SerializeField] private float speedMove = 1f;
    [SerializeField] private float speedRotation = 1f;
    [SerializeField] private Transform myTransform;

    [SerializeField] private float testAngle;
    //speed


    private Queue<Command> CommandQueue;

    bool isOnQueue;

    private void OnEnable()
    {
        EndStep += NextStep;
    }

    private void OnDisable()
    {
        EndStep -= NextStep;
    }

    #region MOVE
    /*
    [Button("forward")]
    private void TestMoveForward()
    {
        MoveForward();
    }

    [Button("forward5")]
    private void TestMoveForward5()
    {
        MoveForward(5);
    }
    */
    public void MoveForward(float length = 1f)
    {
        MakeStepForward(length);
    }

    private void MakeStepForward(float length)
    {
        StartCoroutine(Step(myTransform.position, length));
    }

    private IEnumerator Step(Vector3 before, float length)
    {
        Vector3 target = myTransform.position + myTransform.forward * length;

        float timerMove = speedMove * length;
        for (float f = 0; f < timerMove; f += Time.deltaTime)
        {
            myTransform.position = Vector3.Lerp(before, target, f / timerMove);
            yield return null;
        }

        myTransform.position = Vector3.Lerp(before, target, 1f);

        if (EndStep != null) EndStep();
    }
    #endregion

    #region ROTATION
    /*
    [Button("Backvard")]
    private void TestRotate()
    {
        Rotate();
    }

    [Button("rotate left")]
    private void TestRotateLeft()
    {
        Rotate(-90f);
    }

    [Button("rotate right")]
    private void TestRotateRight()
    {
        Rotate(90f);
    }

    [Button("rotate test")]
    private void TestRotationAtTest()
    {
        Rotate(testAngle);
    }*/

    public void Rotate(float degrees = 180f)
    {
        StartCoroutine(MakeRotation(myTransform.eulerAngles, degrees));
    }

    private IEnumerator MakeRotation(Vector3 before, float angle)
    {
        float timerMove = speedRotation * angle;
        for (float f = 0; f < timerMove; f += Time.deltaTime)
        {
            myTransform.Rotate(Vector3.up * (angle * Time.deltaTime / timerMove));
            yield return null;
        }

        myTransform.eulerAngles = before + Vector3.up * angle;

        if (EndStep != null) EndStep();
    }
    #endregion


    public void AddCommand(Command currentCommand)
    {
        if (CommandQueue == null)
            CommandQueue = new Queue<Command>();

        CommandQueue.Enqueue(currentCommand);
    }

    private void NextStep()
    {
        if (CommandQueue.Count > 0)
        {
            Command currentCommand = CommandQueue.Dequeue();
            switch (currentCommand.CommandType)
            {
                case CommandType.Move: MoveForward(currentCommand.Arg); break;
                case CommandType.Rotation: Rotate(currentCommand.Arg); break;
            }
        }
        else
        {
            isOnQueue = false;
        }
    }

    private void StartQueue()
    {
        if (!isOnQueue)
        {
            isOnQueue = true;
            NextStep();
        }
    }


    public void SaveCommands(List<Command> commands)
    {
        if (CommandQueue == null) CommandQueue = new Queue<Command>();
        CommandQueue.Clear();

        foreach (Command command in commands)
        {
            AddCommand(command);
        }

        StartQueue();
    }
    /*
    [Button("TestSquere")]
    private void TestSquere()
    {
        AddCommand(new Command(CommandType.Move, 5f));
        AddCommand(new Command(CommandType.Rotation, 90f));
        AddCommand(new Command(CommandType.Move, 5f));
        AddCommand(new Command(CommandType.Rotation, 90f));
        AddCommand(new Command(CommandType.Move, 5f));
        AddCommand(new Command(CommandType.Rotation, 90f));
        AddCommand(new Command(CommandType.Move, 5f));
        AddCommand(new Command(CommandType.Rotation, 90f));
        StartQueue();
    }
    */
}
